---
title: "Leaving Google, a FLOSS Journey"
subtitle: "A Google-free life for 3€/month"
date: 2021-07-20
tags: ["FLOSS", "Google", "Android", "WD Notes", "WD Location"]
draft: false
---
Google offers a wide range of useful products and services for free, but it's important to note that's free as in beer, not freedom. This guide explains how to obtain a similar quality of digital life without sacrificing privacy or ideology.
<!--more-->
### Why not Google?
This article is not meant to bash Google. In fact, this is the (inevitable?) result of being a Google-addict. When your contact list, calendar schedule, emails, pictures, notes, etc. are all handled by one company, whose sole purpose is to process that data to sell you other things, you start to realize there might be a problem. And admitting you have a problem is the first step to change. There are countless reasons to make this particular change, but perhaps the most important one is that it proves that change is possible. This post could just as easily be "Leaving Apple, a FLOSS Journey" if that were the ecosystem of your fancy. The Internet was built on free and open protocols that have since been packaged and sold by an eerily limited number of Silicon Valley companies. Change is not only possible, it is essential.

### Search
Google started as a search engine, and it was their absolute dominance in the field that made them what they are today. Since then no big leaps have been made in improving search results, and there has been a leveling of the playing field. [DuckDuckGo](https://duckduckgo.com) is currently just as good of an internet search engine as Google for almost all use cases. Switching default search provider is a click away, and chances are you won't even notice having switched. Even in the unlikely event of a search where results are lacking, it is trivial to open a (private) tab and search in Google. A small step to take, a giant leap towards freedom.

### Google Chrome
As the complexity of web browsers has increased, so has the concentration in the browser market. There are now effectively 3 browser engines in use: Blink (Google), WebKit (Apple) and Gecko (Mozilla). Just as Microsoft did with Internet Explorer in the late 90s, Google and Apple are locking their customers into their ecosystem. For some of the items discussed in this article the best FLOSS alternative is still admittedly not up to par with its Google equivalent, but fortunately in the browser wars there is nothing to lose and much to gain by backing [Firefox](https://www.mozilla.org/firefox/new/). Same performance and sync capabilities as Google/Apple, and it even includes a free Password Manager. 

### Gmail
Google pioneered limitless searchable email, and their promise of free expanding storage coupled with a sleek UI was clearly a winner. The problem is that in the process they made a travesty of email. We're capable of transmitting secure messages in near real-time around the world, and yet we accept that the email provider can go ahead and read it. It's like delivering mail using armored vehicles to then have your doorman read all your letters. There are email providers that are much more privacy-protecting than Google, but why not go the extra mile and become your own email provider. [Mail-in-a-Box](https://mailinabox.email) is the easiest way to set-up your own mail server. Procure a cheap server ([Hetzner CX11](https://www.hetzner.com/cloud) for 3€/month is more than enough), run an installer script, and you're set. The email can be accessed through [Roundcube Webmail](https://roundcube.net), a desktop client such as [Thunderbird](https://www.thunderbird.net), or an Android client like [K-9 Mail](https://k9mail.app).

### Google Contacts
Although Google never managed to build a stronghold in social media, personal contact information and the social graphs that can be formed knowing who has who's details is probably some of the most valuable data they handle. It used to be that consumers had a big yellow book listing contact details of businesses, now Google has a big book with all the personal information of consumers and sells it to businesses. Thankfully, Mail-in-a-Box provides a CardDAV server through [NextCloud](https://nextcloud.com), which can be used to store and sync contacts to Android with [DAVx<sup>5</sup>](https://www.davx5.com). The [AOSP](https://source.android.com) contacts app and the NextCloud web interface are just as good as the Google mobile and web apps.

### Google Calendar
In addition to the email server and CardDAV server, Mail-in-a-Box's NextCloud installation also includes a CalDAV server, to store and sync calendars (again using DAVx<sup>5</sup> to sync on Android). A good FLOSS calendar app for Android is [Etar Calendar](https://github.com/Etar-Group/Etar-Calendar).

### Google Maps
Google brought down the duopoly of TomTom and Garmin by providing a free (as in beer) alternative with unprecedented features like Street View. Now comfortable in a dominant position, the real price reveals itself. It has been openly conceded with the service's recent [pricing changes](https://cloud.google.com/maps-platform/user-guide/pricing-changes/), but it is evident as well in what Google Maps has become: a digital billboard service. [OpenStreetMap](https://www.openstreetmap.org) is a crowd-sourced alternative with open data and a great open source Android app: [OsmAnd](https://osmand.net). It's a shame to lose Google's accurate traffic predictions, but when you realize that it's powered by monitoring millions of people's movements it's easier to accept.

### Google Keep
As with email, there are tons of note providers that probably respect privacy more than Google, but again the cost of self-hosting is negligible against the benefit of controlling your data. NextCloud has a Notes plugin (and an Android app to go with it), but at Ideotec we decided to go with a fully WebDAV-based solution to stick to open protocols and isolate the client and server implementations. [WD Notes](https://gitlab.com/ideotec/wdnotes) is a cross-platform mobile app that stores and syncs notes with any WebDAV server using open formats.

### Google Play Services
Android is open source, but Google has gradually fused their products and services into Android to the point where the operating system's functionality is limited if it is not paired with some of Google's proprietary pieces. It's common to find many Google apps preinstalled on an Android smartphone, but that's easily fixable by substituting them for FLOSS solutions. The real issue is with the background services that many third-party apps rely on to function properly. [MicroG](https://microg.org) is a FLOSS reimplementation of some of these services.

### Google Play Store
Thankfully, Android allows installing apps from different sources other than the Google Play Store. [F-Droid](https://f-droid.org/) is a great app store that only contains open source apps. Instead of submitting an APK, developers submit the code repository and F-Droid compiles the app, guaranteeing that it does exactly what is in the code. [Aurora Store](https://auroraoss.com/download/AuroraStore/) is an open source app that offers the entire Google Play Store catalog.

### Google Maps Timeline
One of the advantages of being continuously tracked by Google is that they share some data with you so that you can check what you were doing at any moment since your first smartphone. No other app provides such a persistent and efficient invasion of privacy, which is why at Ideotec we decided to create [WD Location](https://gitlab.com/ideotec/wdlocation).