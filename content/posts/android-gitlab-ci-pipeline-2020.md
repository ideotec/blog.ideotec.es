---
title: "Android GitLab CI Pipeline in 2020"
subtitle: "Adapting to recent changes in the Android toolchain"
date: 2020-11-24
tags: ["Android", "GitLab", "CI", "T16Fling"]
draft: false
---
You can find examples online of the `.gitlab-ci.yml` file to use for compiling Android apps (including one on [GitLab's Blog](https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/)), but they are all now outdated since Google made several breaking changes to the naming, directory structure, and environment variables. 
<!--more-->
### Android SDK Tools is deprecated

One of the first things the CI scripts do is download the SDK Tools (`sdk-tools-linux` package) from dl.google.com. Well, this package is now [deprecated](https://developer.android.com/studio/releases/sdk-tools), and has been superseded by the new [Command-Line Tools](https://developer.android.com/studio/command-line#tools-sdk). The new URL to download these tools is:
```
https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip
```

### Directory structure changes

While it used to be that the command-line tools could be found in `{android-sdk}/tools/bin/`, they must now reside in `{android-sdk}/cmdline-tools/{version}/bin/`. To make matters more confusing, the zip file downloaded in the previous step creates the following directory structure: `cmdline-tools/bin/`, so there is a missing `{version}` directory in the middle. To keep with the required structure, you must create the `cmdline-tools` directory, extract the zip into that directory (which creates `cmdline-tools/cmdline-tools/bin/`) and then rename the second `cmdline-tools` to `latest` (you could rename it to the version of the tools, but "latest" works). The final directory structure is:
```
{android-sdk}/cmdline-tools/latest/bin/
```

### ANDROID_HOME is deprecated

Once all the SDK tools have been installed the CI script creates the environment variables to point to them. Since the early days of Android its SDK directory was known as `ANDROID_HOME`, but that is now [deprecated](https://developer.android.com/studio/command-line/variables). The new environment variable that points to the Android SDK directory is called `ANDROID_SDK_ROOT`.

### The new GitLab CI configuration file

With all of these changes applied, the `.gitlab-ci.yml` file should now look like this:
```yaml
image: openjdk:8-jdk

variables:
  ANDROID_COMPILE_SDK:    "30"
  ANDROID_BUILD_TOOLS:    "30.0.2"
  ANDROID_CMDLINE_TOOLS:  "6858069_latest"

before_script:
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
  - wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_CMDLINE_TOOLS}.zip
  - mkdir android-sdk-linux
  - unzip -d android-sdk-linux/cmdline-tools android-sdk.zip
  - mv android-sdk-linux/cmdline-tools/cmdline-tools android-sdk-linux/cmdline-tools/latest
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platform-tools" >/dev/null
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
  - export ANDROID_SDK_ROOT=$PWD/android-sdk-linux
  - export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools/
  - chmod +x ./gradlew
  # temporarily disable checking for EPIPE error and use yes to accept all licenses
  - set +o pipefail
  - yes | android-sdk-linux/tools/bin/sdkmanager --licenses
  - set -o pipefail

stages:
  - build

lintDebug:
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint

assembleDebug:
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
    - app/build/outputs/apk/debug/app-debug.apk
```