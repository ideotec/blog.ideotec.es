const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    enabled: true,
    content: ["./layouts/**/*.html"]
  },
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        'cyan': colors.cyan,
        'trueGray': colors.trueGray,
        'rose': colors.rose
      },
      boxShadow: {
        'DEFAULT': 'rgba(0, 0, 0, 0.8) 1px 1px 2px 0px'
      },
      fontSize: {
        '90': '90%'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
